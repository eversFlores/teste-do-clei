# Bem vindo ao teste para desenvolvedor frontend da Coobrastur.

## Deverá ser utilizado a api publica do site: [Super Herois API](https://superheroapi.com).
## O teste poderá ser realizado em Angular JS, Angular 2+ ou React

## Deverá ser criado um fork do repositório atual e após terminado enviar uma pull request pra esse repositório.

- Utilizar o versionamento GIT para desenvolvimento.
- Utilizar orientação a objetos e classes para representar os dados mostrados.
- Elaborar uma tela de login com validações de campo (required, null, undefined) contendo mensagem adequada de validação.
  Não é necessário criar uma autenticação verdadeira com request, só validação de campos mesmo e depois pode redirecionar pra página principal da aplicação.
- Listar os herois com nome, uma breve descrição biográfica e opção de "ver detalhes".
- Nos detalhes dos heróis deve ser inserido os powerstats, biography, appearance e image contidas na API sobre aquele herói.
- Deve ser criado campo de pesquisa na tela da lista onde é possível pesquisar por um Herói

### O teste deve levar em consideração as boas práticas do mercado para estrutura e clean code na aplicação.

### Todo o design ficará a critério do candidato.

### Poderá ser usado quaisquer bibliotecas e framework de apoio para criação do design e validações, mas será levado em consideração a confiabilidade das bibliotecas escolhidas.